=========
Changelog
=========

Version 2.1
===========
*Released 2021-05-13*

**Minor Changes:**

* Added ``Procfile`` and ``runtime.txt`` used to deploy Heroku apps.
* Added checks stage in CI to run the pre-commit checks.
* Added deploy stage to CI to deploy the app to Heroku on tags pushed to master@allerter/twitter-status-bot.
* Add a custom CSS file to Sphinx docs to change the color of the TOC tree captions.

Version 2.0
===========
*Released 2021-05-13*

**New Features:**

* Allow passing username to generate a tweet from the given us
* Limit user queries to once every 5 seconds.

**Major Changes:**

* The bot won't generate a sticker for the effective user anymore. Therefore the user has to include a username at the beginning of the query like `@username` even if they want to generate one for their own account.
* Get user info using a user bot since Bot API won't resolve unseen usernames.
* User has to type "done" at the end of the inline query for the sticker to be generated. This is to avoid trying to resolve the username while the user is still typing.

**Minor changes:**

* Add `telethon` and its optional C extension `cryptg` to dependencies (needed for resolving username and downloading profile photo).
* Bump pre-commit dependencies.
* Remove saving logs to disk.
* Update Mypy config in `setup.cfg`. PTB imports will be checked, but some others ignored.

Version 1.5.2
=============
*Released 2021-03-13*

**New Features:**

* Remember generated stickers and suggest them in inline mode

**Minor changes:**

* Bump dependencies
* Improve error handler
* Use bytes stream instead of temporary files

Version 1.5.1
===========
*Released 2020-08-24*

**Minor changes:**

* Update PTB to v13.0
* Update ``ptbstats`` to v1.3
* Handle hyphenation errors by warning users
* Handle flood control errors by going to sleep

Version 1.5
===========
*Released 2020-08-24*

**Minor changes:**

* Update ``ptbstats`` to v1.2
* Don't accept empty inline queries for stats
* Handle exceptions for updates without ``effective_chat`` better

Version 1.4
===========
*Released 2020-08-16*

**Enhancements:**

* Include the `ptbstats <https://hirschheissich.gitlab.io/ptbstats/>`_ plugin for statistics

Version 1.3
===========
*Released 2020-07-26*

**Bug fixes:**

* Handle messages only when in private chat. The bot apparently was added to some channels, but that just doesn't make any sense.
* Fix failing of documentation build

Version 1.2
===========
*Released 2020-07-18*

Bug fixes:

* Make deletion of stickers from set even more robust
* Handle edited messages

Version 1.1
===========
*Released 2020-06-20*

Bug fixes:

* Make inline results personal for each user
* Make deletion of stickers from set more robust

Version 1.0
===========
*Released 2020-06-19*

Initial release. Adds basic functionality.
