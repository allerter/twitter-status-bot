from io import BytesIO
from typing import Any, Optional, Union

from telegram import User
from telethon.tl.types import ChatPhoto, UserProfilePhoto


class MixinUser(User):
    """
    User with a photo

    Although the class is called MixinUser, channels and group chats returned by
    Telethon's ``client.get_entity`` will fit into this class as well.

    Args:
        *args: Arbitrary args to pass on to ``telegram.User``.
        photo: Optional. User's profile photo data. Defaults to None.
        photo_file: Optional. User's photo file. Defaults to None
        **kwargs (:obj:`Any`, optional): Arbitrary keyword args to pass on to ``telegram.User``.

    Returns:
        :class:`MixinUser`: User with a photo that has a photo ID.

    Note:
        When a ``ChatPhoto`` is passed, the ``photo_id`` will be added to the
        photo object.

    """
    def __init__(self,
                 *args: Any,
                 photo: Optional[Union[UserProfilePhoto, ChatPhoto]] = None,
                 photo_file: Optional[BytesIO] = None,
                 **kwargs: Any):
        super().__init__(*args, **kwargs)
        if isinstance(photo, ChatPhoto):
            photo_id = str(photo.photo_small.local_id) + str(photo.photo_small.volume_id)
            setattr(photo, "photo_id", photo_id)
        self.photo = photo
        self.photo_file = photo_file
