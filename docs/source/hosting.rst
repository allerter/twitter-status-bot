Hosting this bot
----------------

If you like the functionality of this bot and want to host your own instance, simply clone the
repository, set the environment variables below. Then, install the dependencies and you're ready to go!

Environment Variables
=====================

* ``BOT_TOKEN`` - Your bot token
* ``ADMINS_CHAT_ID`` - Your Chat ID
* ``STICKER_SET_NAME`` (Optional) - Name for when the bot generates a new sticker set
* ``API_ID`` - Your Telegram app API ID
* ``API_HASH`` - Your Telegram app API hash
* ``SESSION_STRING`` - Session string used by Telethon to connect to your account
