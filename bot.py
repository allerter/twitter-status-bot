#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Methods for the bot functionality."""
import logging
import time
import traceback
import html
import re
import asyncio
from io import BytesIO
from typing import cast, Dict, Any, Optional
from uuid import uuid4

from telegram import (Update, InlineKeyboardMarkup, InlineKeyboardButton, Bot, StickerSet,
                      ChatAction, User, ChosenInlineResult, InlineQuery, Message,
                      InlineQueryResultCachedSticker)
from telegram.error import BadRequest, RetryAfter
from telegram.ext import CallbackContext, Dispatcher, CommandHandler, MessageHandler, Filters, \
    InlineQueryHandler, ChosenInlineResultHandler
from telegram.utils.helpers import mention_html
from emoji import emojize
from telethon import TelegramClient
from telethon.sessions import StringSession
from telethon.tl.types import Channel, Chat
from telethon.tl.types import User as TelethonUser

from twitter import build_sticker, HyphenationError
from constants import ADMINS_CHAT_ID, SESSION_STRING, API_HASH, API_ID
from constants import STICKER_SET_NAME as st_set_name
from mixinuser import MixinUser

logger = logging.getLogger(__name__)

ADMIN: int = int(ADMINS_CHAT_ID)
""":obj:`int`: Chat ID of the admin as read from ``bot.ini``."""
STICKER_SET_NAME: str = st_set_name
""":obj:`str`: The name of the sticker set used to generate the sticker as read from
``bot.ini``."""
HOMEPAGE: str = 'https://allerter.gitlab.io/twitter-status-bot/'
""":obj:`str`: Homepage of this bot."""
FILE_IDS: str = 'file_ids'
""":obj:`str`: Key for ``user_data`` where sticker ids are stored."""
TEMP_FILE_IDS: str = 'temp_file_ids'
""":obj:`str`: Key for ``user_data`` where temporary sticker ids are stored."""


def info(update: Update, context: CallbackContext) -> None:
    """
    Returns some info about the bot.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    msg = cast(Message, update.message)
    if context.args:
        text = str(HyphenationError())
        keyboard = InlineKeyboardMarkup.from_button(
            InlineKeyboardButton('Try again', switch_inline_query=''))
    else:
        text = emojize(
            ('I\'m <b>Twitter Status Bot Fork</b>. My profession is generating custom'
             ' stickers looking like tweets from any Telegram entity (user, chat or channel).'
             '\n\nTo learn more about me, please visit the original bot\'s Homepage '
             ':slightly_smiling_face:.'
             '\n\n<b>How to use</b>'
             '\ninline mode query: @username text done'
             '\nprivate chat query: @username text'
             '\n\nPlease note that you can only get stickers once every 5 seconds.'),
            use_aliases=True)

        keyboard = InlineKeyboardMarkup.from_button(
            InlineKeyboardButton(emojize('Twitter Status Fork Bot :robot_face:', use_aliases=True),
                                 url=HOMEPAGE))

    msg.reply_text(text, reply_markup=keyboard)


def error(update: Update, context: CallbackContext) -> None:
    """
    Informs the originator of the update that an error occured and forwards the traceback to the
    admin.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    # Log the error before we do anything else, so we can see it even if something breaks.
    logger.error(msg="Exception while handling an update:", exc_info=context.error)

    if isinstance(context.error, RetryAfter):
        time.sleep(int(context.error.retry_after) + 2)
        return

    # Inform sender of update, that something went wrong
    if isinstance(update, Update) and update.effective_message:
        text = emojize('Something went wrong :worried:. I informed the admin :nerd_face:.',
                       use_aliases=True)
        update.effective_message.reply_text(text)

    # Get traceback
    exception = cast(Exception, context.error)
    tb_list = traceback.format_exception(None, exception, exception.__traceback__)
    trace = ''.join(tb_list)

    # Gather information from the update
    payload = ''
    if isinstance(update, Update):
        if update.effective_user:
            payload += ' with the user {}'.format(
                mention_html(update.effective_user.id, update.effective_user.first_name))
        if update.effective_chat and update.effective_chat.username:
            payload += f' (@{html.escape(update.effective_chat.username)})'
        if update.poll:
            payload += f' with the poll id {update.poll.id}.'
        text = f'Hey.\nThe error <code>{html.escape(str(context.error))}</code> happened' \
               f'{payload}. The full traceback:\n\n<code>{html.escape(trace)}</code>'

    # Send to admin
    context.bot.send_message(ADMIN, text)


def build_sticker_set_name(bot: Bot) -> str:
    """
    Builds the sticker set name given by ``STICKER_SET_NAME`` for the given bot.

    Args:
        bot: The Bot owning the sticker set.

    Returns:
        str
    """
    return '{}_by_{}'.format(STICKER_SET_NAME, bot.username)


def get_sticker_set(bot: Bot, name: str) -> StickerSet:
    """
    Get's the sticker set and creates it, if needed.

    Args:
        bot: The Bot owning the sticker set.
        name: The name of the sticker set.

    Returns:
        StickerSet
    """
    try:
        return bot.get_sticker_set(name)
    except BadRequest as e:
        if 'invalid' in str(e):
            with open('./logo/TwitterStatusBot-round.png', 'rb') as sticker:
                bot.create_new_sticker_set(ADMIN, name, STICKER_SET_NAME, '🐦', png_sticker=sticker)
            return bot.get_sticker_set(name)
        else:
            raise e


def clean_sticker_set(bot: Bot) -> None:
    """
    Cleans up the sticker set, i.e. deletes all but the first sticker.

    Args:
        bot: The bot.
    """
    sticker_set = get_sticker_set(bot, build_sticker_set_name(bot))
    if len(sticker_set.stickers) > 1:
        for sticker in sticker_set.stickers[1:]:
            try:
                bot.delete_sticker_from_set(sticker.file_id)
            except BadRequest as e:
                if 'Stickerset_not_modified' in str(e):
                    pass
                else:
                    raise e


def get_sticker_id(text: str, user: MixinUser, context: CallbackContext) -> str:
    """
    Gives the sticker ID for the requested sticker.

    Args:
        text: The text to display on the tweet.
        user: The user the tweet is created for.
        context: The callback context as provided by the dispatcher.

    Returns:
        str: The stickers file ID
    """
    bot = context.bot

    clean_sticker_set(context.bot)

    sticker_set_name = build_sticker_set_name(bot)
    emojis = '🐦'

    sticker_stream = BytesIO()
    sticker = build_sticker(text, user, context)
    sticker.save(sticker_stream, format='PNG')
    sticker_stream.seek(0)

    get_sticker_set(bot, sticker_set_name)
    bot.add_sticker_to_set(ADMIN, sticker_set_name, emojis, png_sticker=sticker_stream)

    sticker_set = get_sticker_set(bot, sticker_set_name)
    sticker_id = sticker_set.stickers[-1].file_id

    return sticker_id


def get_user(username: str, context: CallbackContext) -> MixinUser:
    """
    Gets user data by resolving username.

    Args:
        username: Entity's username.
        context: The callback context as provided by the dispatcher.

    Raises:
        ValueError: If username isn't found or leads to an unknown entity
            (anything except User, Chat, or Channel).

    Returns:
        MixinUser
    """
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    with TelegramClient(
            StringSession(SESSION_STRING),
            api_id=API_ID,
            api_hash=API_HASH,
    ) as client:
        user_data = loop.run_until_complete(client.get_entity(username))
        if isinstance(user_data, TelethonUser):
            is_bot = user_data.bot
            first_name = user_data.first_name
            last_name = user_data.last_name
        elif isinstance(user_data, (Chat, Channel)):
            is_bot = False
            first_name = user_data.title
            last_name = None
        else:
            raise ValueError("Unknown entity.")
        username = user_data.username
        photo = user_data.photo
        if photo is not None:
            photo_file = loop.run_until_complete(
                client.download_profile_photo(
                    user_data,
                    file=BytesIO(),
                    download_big=False,
                ))
        else:
            photo_file = None
        user = MixinUser(
            id=user_data.id,
            is_bot=is_bot,
            first_name=first_name,
            last_name=last_name,
            username=username,
            photo=photo,
            photo_file=photo_file,
            bot=context.bot,
        )
    loop.close()
    return user


def message(update: Update, context: CallbackContext) -> None:
    """
    Answers a text message by providing the requested sticker.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    msg = cast(Message, update.effective_message)
    text = cast(str, msg.text)
    user_data = cast(dict, context.user_data)
    effective_user = cast(User, update.effective_user)
    usernames: Optional[re.Match] = re.search(r"@[A-Za-z0-9_]*", msg.text)
    if usernames is None:
        msg.reply_text("No username found in message.")
        return
    username: str = usernames.group(0)
    text = text.replace(username, "").strip()
    last_query = user_data.get("last_query")
    if last_query and (time_elapsed := time.time() - last_query) < 5:
        msg.reply_text(f"Please wait {5 - int(time_elapsed)} seconds.")
        return
    try:
        user = get_user(username, context)
        context.bot.send_chat_action(effective_user.id, ChatAction.UPLOAD_PHOTO)
        file_id = get_sticker_id(text, user, context)
        msg.reply_sticker(file_id)
        user_data.setdefault(FILE_IDS, []).append(file_id)
    except ValueError as e:
        msg.reply_text(str(e))
    except HyphenationError as e:
        msg.reply_text(str(e))
    finally:
        user_data["last_query"] = time.time()
    clean_sticker_set(context.bot)


def inline(update: Update, context: CallbackContext) -> None:
    """
    Answers an inline query by providing the requested sticker.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    inline_query = cast(InlineQuery, update.inline_query)
    user_data = cast(dict, context.user_data)
    query = inline_query.query

    file_ids = user_data.setdefault(FILE_IDS, [])
    kwargs: Dict[str, Any] = {
        'results': [
            InlineQueryResultCachedSticker(id=f'tweet {i}', sticker_file_id=sticker_id)
            for i, sticker_id in enumerate(reversed(file_ids))
        ]
    }

    if query:
        if not query.endswith("done"):
            return
        last_query = user_data.get("last_query")
        if last_query and time.time() - last_query < 5:
            return
        try:
            usernames = re.search(r"@[A-Za-z0-9_]*", query)
            if usernames is not None:
                username = usernames.group(0)
                text = query.replace(username, "").replace("done", "").strip()
                user = get_user(username, context)
            else:
                raise ValueError("No username specified.")
            file_id = get_sticker_id(text, user, context)
            key = str(uuid4())
            user_data.setdefault(TEMP_FILE_IDS, {})[key] = file_id
            kwargs['results'].insert(
                0, InlineQueryResultCachedSticker(id=key, sticker_file_id=file_id))
        except HyphenationError:
            kwargs['results'] = []
            kwargs['switch_pm_text'] = 'Click me!'
            kwargs['switch_pm_parameter'] = 'hyphenation_error'
        except ValueError as e:
            kwargs['results'] = []
            kwargs['switch_pm_text'] = str(e)
            kwargs['switch_pm_parameter'] = 'value_error'
        finally:
            user_data["last_query"] = time.time()

    try:
        inline_query.answer(**kwargs, is_personal=True, auto_pagination=True, cache_time=0)
    except BadRequest as e:
        if 'Query is too old' in str(e):
            pass
        else:
            raise e

    clean_sticker_set(context.bot)


def handle_chosen_inline_result(update: Update, context: CallbackContext) -> None:
    """
    Appends the chosen sticker ID to the users corresponding list.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    result_id = cast(ChosenInlineResult, update.chosen_inline_result).result_id

    user_data = cast(dict, context.user_data)
    if not result_id.startswith('tweet'):
        temp_dict = user_data.setdefault(TEMP_FILE_IDS, {})
        file_id = temp_dict.get(result_id)
        if file_id:
            user_data.setdefault(FILE_IDS, []).append(file_id)
        temp_dict.clear()


def default_message(update: Update, context: CallbackContext) -> None:
    """
    Answers any message with a note that it could not be parsed.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    cast(Message,
         update.effective_message).reply_text('Sorry, but I can only convert text messages. '
                                              'Send "/help" for more information.')


def register_dispatcher(disptacher: Dispatcher) -> None:
    """
    Adds handlers. Convenience method to avoid doing that all in the main script.
    Also sets the bot commands.

    Args:
        disptacher: The :class:`telegram.ext.Dispatcher`.
    """
    dp = disptacher

    # error handler
    dp.add_error_handler(error)

    # basic command handlers
    dp.add_handler(CommandHandler(['start', 'help'], info))

    # functionality
    dp.add_handler(MessageHandler(Filters.text & Filters.chat_type.private, message))
    dp.add_handler(MessageHandler(Filters.all & Filters.chat_type.private, default_message))
    dp.add_handler(InlineQueryHandler(inline))
    dp.add_handler(ChosenInlineResultHandler(handle_chosen_inline_result))

    # Bot commands
    dp.bot.set_my_commands([['help', 'Displays a short info message about the Twitter Status Bot'],
                            ['start', 'See "/help"']])
