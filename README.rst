Twitter Status Bot Fork
=======================

.. image:: https://img.shields.io/badge/python-3.8-blue
   :target: https://www.python.org/doc/versions/
   :alt: Supported Python Versions

.. image:: https://img.shields.io/badge/backend-python--telegram--bot-blue
   :target: https://python-telegram-bot.org/
   :alt: Backend: python-telegram-bot

.. image:: https://img.shields.io/badge/documentation-is%20here-orange
   :target: https://allerter.gitlab.io/twitter-status-bot/
   :alt: Documentation

.. image:: https://img.shields.io/badge/chat%20on-Telegram-blue
   :target: https://t.me/TwitterStatusForkBot
   :alt: Telegram Chat

»Twitter Status Bot Fork« is a fork of a simple Telegram Bot that let's you create stickers looking like Tweets on the fly for any user by inputing their username.
You can find it at `@TwitterStatusForkBot`_.
Originally inspired by this `sticker set`_ by `@AboutTheDot`_.

.. _`@TwitterStatusForkBot`: https://t.me/TwitterStatusForkBot
.. _`sticker set`: https://t.me/addstickers/TweetsByTelegramD
.. _`@AboutTheDot`: https://t.me/AboutTheDot/19
