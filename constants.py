import os


BOT_TOKEN = os.environ["BOT_TOKEN"]
ADMINS_CHAT_ID = os.environ["ADMINS_CHAT_ID"]
STICKER_SET_NAME = os.environ.get("STICKER_SET_NAME", "StickerGenerationSetFork")
SESSION_STRING = os.environ["SESSION_STRING"]
API_ID = os.environ["API_ID"]
API_HASH = os.environ["API_HASH"]
